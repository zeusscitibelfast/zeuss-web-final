import { Component, OnInit } from '@angular/core';
import { StrategyService } from '../strategy.service';
import { LivepriceService } from '../liveprice.service';
import { Strategy } from '../strategy';
import { environment } from '../../environments/environment';



@Component({
  selector: 'app-strategy',
  templateUrl: './strategy.component.html',
  styleUrls: ['./strategy.component.css']
})
export class StrategyComponent implements OnInit {

  stocks: Object[] = [{ticker: "AAPL"}, {ticker: "MSFT"}, {ticker: "GOOG"}, {ticker: "BRK-A"}, {ticker: "NSC"}]
  prices:any
  bollingers:any

  apiURL:string = environment.rest_host


  ticker_name:string
  size:number
  exitProfitLoss:number
  upperBand: number
  lowerBand: number
  strategy: Strategy
  strategyStats:any


  
  constructor(private strategyService:StrategyService, private livepriceService:LivepriceService) { }

  

  // invokeService(){
  //   this.strategyService.getAllBollingers().subscribe( (result) => {
  //     this.bollingers = result
  //     console.log(result)
  //   })
  // }

  invokeLivePrice(){
    this.livepriceService.getLivePrices().subscribe ( (result)=>{
      this.prices=result
      console.log(result)
    })
  }

  getStrategies(){
    this.strategyService.getAllStrategies().subscribe( (result) => {
      this.strategyStats = result
      console.log(result)
    })
  }


  ngOnInit() {
    //this.invokeService()
    setInterval(()=>{this.invokeLivePrice()}, 3000)
    this.getStrategies()
  }

  handleClick(){
    console.log("form entry, ticker" +this.ticker_name)
    console.log("form entry, size: "+ this.size)
    console.log("form entry, exitProfitLoss " + this.exitProfitLoss)
    console.log("form entry, lower band " + this.lowerBand)
    console.log("form entry, upper band " + this.upperBand)
    this.strategy = new Strategy(this.exitProfitLoss, this.size, this.ticker_name, this.lowerBand, this.upperBand);
    this.strategyService.createStrategy(this.strategy).subscribe(result =>
      {
        console.log("this.strategy: " + JSON.stringify(this.strategy))

      }
     
    );
    window.location.reload();
    
  }
  



}
