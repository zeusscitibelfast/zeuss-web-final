import { Component, OnInit } from '@angular/core';
import { TradesService } from '../trades.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-trades',
  templateUrl: './trades.component.html',
  styleUrls: ['./trades.component.css']
})
export class TradesComponent implements OnInit {
 
  searchedTrades:any
  model:any
  tradeId:number

  constructor(private tradesPrice:TradesService) { }

  invokeService(){
    this.tradesPrice.getAllTradesData().subscribe( (result) => {
      this.model = result
      console.log(result)
    })
  }

  ngOnInit() {
    
  }

  listTrades(){
    this.invokeService()
  }

  searchTrades(){
    this.tradesPrice.getParamData(this.tradeId).subscribe( (result) => {
      this.searchedTrades = result
      console.log(result)
    })
  }

}
