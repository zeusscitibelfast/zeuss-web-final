import { Stock } from './stock'
export class Strategy{
    exitProfitLoss: number
    size: number
    lowerBand:number
    upperBand:number
    stock: Stock

    constructor(exitProfitLoss, size, ticker_name, lowerBand, upperBand){
        this.size = size
        this.exitProfitLoss = exitProfitLoss
        this.lowerBand = lowerBand
        this.upperBand = upperBand
        this.stock = new Stock(ticker_name)
        
    }
}