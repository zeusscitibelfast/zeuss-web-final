import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TradesComponent } from './trades/trades.component';
import { HomeComponent } from './home/home.component';
import { StrategyComponent } from './strategy/strategy.component';
import { MetricsComponent } from './metrics/metrics.component';



const routes: Routes = [ { path: 'trades', component:TradesComponent }, {path: '', component:HomeComponent}, {path: 'strategy', component: StrategyComponent}, {path: 'metrics', component:MetricsComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
