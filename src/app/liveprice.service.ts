import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LivepriceService {
    
    apiURL:string = environment.rest_host

  constructor(private http:HttpClient) { }

  private handleError<T> (Operation = 'operation',result?: T){
    return (error:any): Observable<T> =>{
      console.error(error);
      alert("This item could not be found")
      return throwError(
        "Your request could not be found")
    };
  }

  getLivePrices(){
    return this.http.get(`${this.apiURL}${'/price'}`).pipe(
      catchError(this.handleError<Object[]>('getLivePrices' , []))
    );
  }
}
