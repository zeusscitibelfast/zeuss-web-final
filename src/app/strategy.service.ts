import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError} from 'rxjs';
import { environment } from '../environments/environment';
import { Strategy } from './strategy';

@Injectable({
  providedIn: 'root'
})
export class StrategyService {
  apiURL = environment.rest_host;
  

  constructor(private http:HttpClient) { }

  private handleError<T> (Operation = 'operation',result?: T){
    return (error:any): Observable<T> =>{
      console.error(error);
      alert("This item could not be found")
      return throwError(
        "Your request could not be found")
    };
  }



  createStrategy(strategy: Strategy){
    return this.http.post<Strategy>(`${this.apiURL}/${'strategy'}`, strategy);
  }

  getAllStrategies(){
    return this.http.get(`${this.apiURL}${'/strategy'}`)
    .pipe(catchError(this.handleError<Object[]>('getAllStrategies', []))
    );
  }

  
  
}


