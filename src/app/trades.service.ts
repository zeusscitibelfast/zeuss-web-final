import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError} from 'rxjs';
import { environment } from '../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class TradesService {
  apiURL = environment.rest_host
  

  constructor(private http:HttpClient) { }

  private handleError<T> (Operation = 'operation',result?: T){
    return (error:any): Observable<T> =>{
      console.error(error);
      alert("This item could not be found")
      return throwError(
        "Your request could not be found")
    };
  }
    
  //getting all trade  data
  getAllTradesData(){
    return this.http.get(`${this.apiURL}${'/trade'}`)
    .pipe(catchError(this.handleError<Object[]>('getData', []))
    );
  }

  getParamData(tradeId){
    return this.http.get(`${this.apiURL}${'/trade'}/${tradeId}`).pipe(
      catchError(this.handleError<Object[]>('getParamData', []))
    );
  }
  
}
